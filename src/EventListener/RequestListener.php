<?php

namespace Rest\EventListener;

use Symfony\Component\HttpKernel\Event\RequestEvent;

/**
 * Class RequestListener
 * @package App\EventListener
 */
class RequestListener
{
    private const JSON_TYPE = 'json';

    /**
     * @param RequestEvent $requestEvent
     */
    public function onKernelRequest(RequestEvent $requestEvent)
    {
        $content = $requestEvent->getRequest()->getContent();

        $type = $requestEvent->getRequest()->getContentType();

        if (self::JSON_TYPE === $type) {
            $data = json_decode($content, true);

            foreach ($data as $key => $value) {
                $requestEvent->getRequest()->request->set($key, $value);
            }
        }
    }
}