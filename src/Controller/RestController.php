<?php

namespace Rest\Controller;

use DataMapper\MapperInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class RestController extends AbstractController
{
    /**
     * @var MapperInterface
     */
    protected $mapper;

    /**
     * MeController constructor.
     * @param MapperInterface $mapper
     */
    public function __construct(MapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @param $source
     * @param string|null $destination
     * @param int $code
     *
     * @return JsonResponse
     */
    public function getResponse($source, ?string $destination = null, int $code = JsonResponse::HTTP_OK): JsonResponse
    {
        if (null !== $destination) {
            $dto = $this->mapper->convert($source, $destination);
            return new JsonResponse($this->mapper->extract($dto), $code);
        }

        return new JsonResponse($source, $code);
    }
}